package com.gildedrose.itemExtended;

import com.gildedrose.Item;
import com.gildedrose.itemCalculator.ItemCalculator;

public class ItemExtended extends Item {

    protected ItemCalculator itemCalculator;

    public ItemExtended(String name, int sellIn, int quality, ItemCalculator itemCalculator) {
        super(name, sellIn, quality);
        this.itemCalculator = itemCalculator;
    }

    protected void addQuality() {
        if (itemCalculator.isUnderMaxQuality()) {
            quality += 1;
        }
    }

    protected void substractQuality() {
        if (itemCalculator.isOverMinQuality()) {
            quality -= 1;
        }
    }

    public int substractSellIn() {
        sellIn -= 1;
        return sellIn;
    }

    public int calculateQuality() {
        substractQuality();
        if (itemCalculator.isDueDated())
            substractQuality();
        return quality;
    }

    public void resetQuality() {
        quality = 0;
    }
}
