package com.gildedrose.itemExtended;

import com.gildedrose.itemCalculator.ItemCalculator;

public class ItemSulfuras extends ItemExtended {
    public ItemSulfuras(String name, int sellIn, int quality, ItemCalculator itemCalculator) {
        super(name, sellIn, quality, itemCalculator);
    }

    @Override
    public int calculateQuality() {
        return quality;
    }

    @Override
    public int substractSellIn() {
        return sellIn;
    }

}
