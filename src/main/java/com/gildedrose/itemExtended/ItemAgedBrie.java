package com.gildedrose.itemExtended;

import com.gildedrose.itemCalculator.ItemCalculator;

public class ItemAgedBrie extends ItemExtended {
    public ItemAgedBrie(String name, int sellIn, int quality, ItemCalculator itemCalculator) {
        super(name, sellIn, quality, itemCalculator);
    }

    @Override
    public int calculateQuality() {
        addQuality();
        if (itemCalculator.isDueDated()) {
            addQuality();
        }
        return quality;
    }
}
