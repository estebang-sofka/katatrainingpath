package com.gildedrose.itemExtended;

import com.gildedrose.itemCalculator.ItemCalculator;

public class ItemBackstage extends ItemExtended {

    public ItemBackstage(String name, int sellIn, int quality, ItemCalculator itemCalculator) {
        super(name, sellIn, quality, itemCalculator);
    }

    @Override
    public int calculateQuality() {
        addQuality();
        if (itemCalculator.isDueDated())
            resetQuality();
        else
            addQualityBySellIn();
        return quality;
    }

    private void addQualityBySellIn() {
        if (itemCalculator.isTenDaysOrLess())
            addQuality();
        if (itemCalculator.isFiveDaysOrLess())
            addQuality();
    }
}
