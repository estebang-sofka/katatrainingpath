package com.gildedrose.itemExtended;

import com.gildedrose.itemCalculator.ItemCalculator;

public class ItemConjured extends ItemExtended {
    public ItemConjured(String name, int sellIn, int quality, ItemCalculator itemCalculator) {
        super(name, sellIn, quality, itemCalculator);
    }

    @Override
    public int calculateQuality() {
        substractQuality();
        substractQuality();
        if (itemCalculator.isDueDated()) {
            substractQuality();
            substractQuality();
        }
        return quality;
    }
}
