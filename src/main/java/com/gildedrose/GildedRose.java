package com.gildedrose;

import com.gildedrose.itemCalculator.ItemCalculator;
import com.gildedrose.itemExtended.*;

class GildedRose {

    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item item : items) {
            ItemCalculator itemCalculator = new ItemCalculator(item);
            ItemExtended itemExtended;

            if (itemCalculator.isBackstage())
                itemExtended = new ItemBackstage(item.name, item.sellIn, item.quality, itemCalculator);
            else if (itemCalculator.isSulfuras())
                itemExtended = new ItemSulfuras(item.name, item.sellIn, item.quality, itemCalculator);
            else if (itemCalculator.isAgedBrie())
                itemExtended = new ItemAgedBrie(item.name, item.sellIn, item.quality, itemCalculator);
            else if (itemCalculator.isConjured())
                itemExtended = new ItemConjured(item.name, item.sellIn, item.quality, itemCalculator);
            else
                itemExtended = new ItemExtended(item.name, item.sellIn, item.quality, itemCalculator);

            item.quality = itemExtended.calculateQuality();
            item.sellIn = itemExtended.substractSellIn();
        }
    }
}

