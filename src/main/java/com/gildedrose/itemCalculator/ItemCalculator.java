package com.gildedrose.itemCalculator;

import com.gildedrose.Item;

public class ItemCalculator {
    private static final String AGED_BRIE = "Aged Brie";
    private static final String BACKSTAGE_PASSES_TO_A_TAFKAL_80_ETC_CONCERT = "Backstage passes to a TAFKAL80ETC concert";
    private static final String SULFURAS_HAND_OF_RAGNAROS = "Sulfuras, Hand of Ragnaros";
    private static final String CONJURED_MANA_CAKE = "Conjured Mana Cake";
    private static final int MAX_QUALITY = 50;
    private static final int MIN_QUALITY = 0;
    private boolean isDueDated;
    private boolean isAgedBrie;
    private boolean isBackstage;
    private boolean isSulfuras;
    private boolean isConjured;
    private boolean isUnderMaxQuality;
    private boolean isOverMinQuality;
    private boolean isTenDaysOrLess;
    private boolean isFiveDaysOrLess;

    public ItemCalculator(Item item) {
        initFields(item);
    }

    private void initFields(Item item) {
        isDueDated = item.sellIn < 0;
        isAgedBrie = item.name.equals(AGED_BRIE);
        isBackstage = item.name.equals(BACKSTAGE_PASSES_TO_A_TAFKAL_80_ETC_CONCERT);
        isSulfuras = item.name.equals(SULFURAS_HAND_OF_RAGNAROS);
        isConjured = item.name.equals(CONJURED_MANA_CAKE);
        isUnderMaxQuality = item.quality < MAX_QUALITY;
        isOverMinQuality = item.quality > MIN_QUALITY;
        isTenDaysOrLess = item.sellIn < 11;
        isFiveDaysOrLess = item.sellIn < 6;
    }

    public boolean isDueDated() {
        return isDueDated;
    }

    public boolean isAgedBrie() {
        return isAgedBrie;
    }

    public boolean isBackstage() {
        return isBackstage;
    }

    public boolean isSulfuras() {
        return isSulfuras;
    }

    public boolean isConjured() { return isConjured; }

    public boolean isUnderMaxQuality() {
        return isUnderMaxQuality;
    }

    public boolean isOverMinQuality() {
        return isOverMinQuality;
    }

    public boolean isTenDaysOrLess() { return isTenDaysOrLess; }

    public boolean isFiveDaysOrLess() { return isFiveDaysOrLess; }
}
